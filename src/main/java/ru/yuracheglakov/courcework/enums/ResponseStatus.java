package ru.yuracheglakov.courcework.enums;

import java.io.Serializable;

public enum ResponseStatus implements Serializable {
    YES(0, "Подверждена"),
    NO(1, "Отклонена"),
    NULL(2, "На подверждении");

    private Integer index;
    private String title;

    ResponseStatus(Integer index, String title) {
        this.index = index;
        this.title = title;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
