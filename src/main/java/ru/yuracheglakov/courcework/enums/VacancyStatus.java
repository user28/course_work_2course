package ru.yuracheglakov.courcework.enums;

import java.io.Serializable;

public enum VacancyStatus implements Serializable {
    ACTIVE(1, "Актуально"),
    DEACTIVE(0, "Неактуально");

    int id;
    String title;

    VacancyStatus(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
