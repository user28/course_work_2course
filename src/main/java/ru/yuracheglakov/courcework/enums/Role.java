package ru.yuracheglakov.courcework.enums;

import java.io.Serializable;

public enum Role implements Serializable {
    ADMIN(0L, "Администратор", true),
    APPLICANT(1L, "Соискатель", false),
    EMPLOYER(2L, "Работодатель", false);

    Long id;
    String title;
    Boolean isManager;

    Role(Long id, String title, boolean isManager) {
        this.id = id;
        this.title = title;
        this.isManager = isManager;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isManager() {
        return isManager;
    }

    public void setManager(Boolean isManager) {
        this.isManager = isManager;
    }
}
