package ru.yuracheglakov.courcework.util;

public class Defaults {
    public static final String DATABASE_NAME = "cource_work";
    public static final String APPLICANTS_SCHEMA = "applicants";
    public static final String EMPLOYERS_SCHEMA = "employers";
    public static final String DICTIONARY_SCHEMA = "dictionary";
}
