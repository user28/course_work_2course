package ru.yuracheglakov.courcework.util;

public class Errors {
    public static final Integer EMPLOYER_PHONE_ERROR = 1;
    public static final Integer EMPLOYER_EMAIL_ERROR = 2;
    public static final Integer EMPLOYER_NAME_ERROR = 3;
    public static final Integer PERSON_NAME_ERROR = 4;
    public static final Integer PERSON_SURNAME_ERROR = 5;
    public static final Integer PERSON_PHONE_ERROR = 6;
    public static final Integer PERSON_EMAIL_ERROR = 7;

    public static boolean checkEmail(String email) {
        String emailReg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

        return email.matches(emailReg);
    }

    public static boolean checkPhoneNumber(String phoneNumber) {
        String phoneNumberReg = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$";

        return phoneNumber.matches(phoneNumberReg);
    }

    public static boolean checkLatin(String text) {
        String reg = "[^a-zA-Z0-9]*";

        return text.matches(reg);
    }
}
