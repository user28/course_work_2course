package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.ReponseRepository;
import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Response;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class ResponseService {

    private final ReponseRepository repository;

    @Autowired
    public ResponseService(ReponseRepository repository) {
        this.repository = repository;
    }

    public Response find(Long id) throws ServiceException {
        Optional<Response> personOptional = repository.findById(id);
        return personOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %d", id)));
    }

    public List<Response> findAll() {
        return repository.findAll();
    }

    public Integer count(Vacancy vacancy) {
        return repository.countAllByVacancy(vacancy);
    }

    public Response save(Response fillResponse) {
        return repository.save(fillResponse);
    }

    public Response find(Vacancy vacancy, Applicant applicant) throws ServiceException {
        Optional<Response> personOptional = repository.findByVacancyAndApplicant(vacancy, applicant);
        return personOptional.orElseThrow(() -> new ServiceException("Не найдена запись c"));
    }

    public List<Response> find(Employer employer) {
        return repository.findAllByVacancy_Employer(employer);
    }

    public List<Response> find(Vacancy vacancy, Employer employer) {
        if (null == vacancy)
            return find(employer);

        return repository.findAllByVacancyAndVacancy_Employer(vacancy, employer);
    }

    public boolean isExist(Vacancy vacancy, Applicant applicant) {
        return repository.existsByApplicantAndVacancy(applicant, vacancy);
    }
}
