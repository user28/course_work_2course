package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.WorkSheduleRepository;
import ru.yuracheglakov.courcework.domain.WorkShedule;
import ru.yuracheglakov.courcework.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class WorkSheduleService {

    private final WorkSheduleRepository repository;

    @Autowired
    public WorkSheduleService(WorkSheduleRepository repository) {
        this.repository = repository;
    }

    public WorkShedule find(Long id) throws ServiceException {
        Optional<WorkShedule> personOptional = repository.findById(id);
        return personOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %d", id)));
    }

    public List<WorkShedule> findAll() {
        return repository.findAll();
    }
}
