package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.domain.Person;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Service
public class ProcedureInvoker {

    private final EntityManager entityManager;

    @Autowired
    public ProcedureInvoker(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Long addUser(Person person) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("dictionary.addUser");

        storedProcedureQuery.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(6, Long.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(7, Long.class, ParameterMode.INOUT);


        storedProcedureQuery.setParameter(1, person.getEmail());
        storedProcedureQuery.setParameter(2, person.getPassword());
        storedProcedureQuery.setParameter(3, person.getName());
        storedProcedureQuery.setParameter(4, person.getSurName());
        storedProcedureQuery.setParameter(5, person.getPhone());
        storedProcedureQuery.setParameter(6, person.getRole().getId());
        storedProcedureQuery.setParameter(7, 1L);

        return (Long) storedProcedureQuery.getOutputParameterValue(7);
    }

    Long check(String email, String password) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("dictionary.login");

        storedProcedureQuery.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter(3, Long.class, ParameterMode.INOUT);

        storedProcedureQuery.setParameter(1, email);
        storedProcedureQuery.setParameter(2, password);
        storedProcedureQuery.setParameter(3, 1L);

        return (Long) storedProcedureQuery.getOutputParameterValue(3);
    }
}
