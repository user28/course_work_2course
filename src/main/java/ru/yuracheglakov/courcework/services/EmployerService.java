package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.EmployerRepository;
import ru.yuracheglakov.courcework.domain.Employer;

@Service
public class EmployerService {

    private final EmployerRepository employerRepository;

    @Autowired
    public EmployerService(EmployerRepository employerRepository) {
        this.employerRepository = employerRepository;
    }

    public Employer create(Employer employer) {
        return employerRepository.saveAndFlush(employer);
    }

    public Employer findByContantPerson(Long id) {
        return employerRepository.findByContactPerson_Id(id);
    }
}
