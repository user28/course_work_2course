package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.ApplicantRepository;
import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Person;
import ru.yuracheglakov.courcework.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class ApplicantService {

    private final ApplicantRepository repository;

    @Autowired
    public ApplicantService(ApplicantRepository repository) {
        this.repository = repository;
    }

    public Applicant find(Long id) throws ServiceException {
        Optional<Applicant> applicantOptional = repository.findById(id);
        return applicantOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %d", id)));
    }

    public Applicant find(Person person) throws ServiceException {
        Optional<Applicant> applicantOptional = repository.findByPerson(person);
        return applicantOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c person = %s", person.getEmail())));
    }

    public List<Applicant> findAll() {
        return repository.findAll();
    }

    public Applicant save(Applicant fillApplicant) {
        return repository.save(fillApplicant);
    }
}
