package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final PersonService personService;

    private static ProcedureInvoker procedureInvoker;

    @Autowired
    public AuthService(PersonService personService, ProcedureInvoker procedureInvoker) {
        this.personService = personService;
        AuthService.procedureInvoker = procedureInvoker;
    }

    public static Long isCorrect(String login, char[] password) {
        return procedureInvoker.check(login, new String(password));
    }
}
