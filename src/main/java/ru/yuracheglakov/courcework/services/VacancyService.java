package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.VacancyRepository;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.enums.VacancyStatus;
import ru.yuracheglakov.courcework.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class VacancyService {

    private final VacancyRepository repository;

    @Autowired
    public VacancyService(VacancyRepository repository) {
        this.repository = repository;
    }

    public Vacancy find(Long id) throws ServiceException {
        Optional<Vacancy> vacancyOptional = repository.findById(id);
        return vacancyOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %d", id)));
    }

    public List<Vacancy> findAll(VacancyStatus vacancyStatus) {
        return repository.findAllByStatusIs(vacancyStatus);
    }

    public List<Vacancy> findAll(Employer employer) {
        return repository.findAllByEmployer(employer);
    }

    public List<Vacancy> findAll() {
        return repository.findAll();
    }

    public Vacancy save(Vacancy vacancy) {
        return repository.saveAndFlush(vacancy);
    }

    public Vacancy find(String name, Employer employer) throws ServiceException {
        Optional<Vacancy> vacancyOptional = repository.findByPosition_NameAndEmployer(name, employer);
        return vacancyOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %s", name)));
    }
}
