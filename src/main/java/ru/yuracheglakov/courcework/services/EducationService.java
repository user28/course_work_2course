package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.EducationRepository;
import ru.yuracheglakov.courcework.domain.Education;
import ru.yuracheglakov.courcework.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class EducationService {

    private final EducationRepository repository;

    @Autowired
    public EducationService(EducationRepository repository) {
        this.repository = repository;
    }

    public Education find(Long id) throws ServiceException {
        Optional<Education> educationOptional = repository.findById(id);
        return educationOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %d", id)));
    }

    public List<Education> findAll() {
        return repository.findAll();
    }

    public Education find(String degree) throws ServiceException {
        Optional<Education> educationOptional = repository.findByDegreeEquals(degree);
        return educationOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %s", degree)));
    }
}
