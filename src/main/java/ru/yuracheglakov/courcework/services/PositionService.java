package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.PositionRepository;
import ru.yuracheglakov.courcework.domain.Position;
import ru.yuracheglakov.courcework.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class PositionService {

    private final PositionRepository repository;

    @Autowired
    public PositionService(PositionRepository repository) {
        this.repository = repository;
    }

    public Position find(Long id) throws ServiceException {
        Optional<Position> personOptional = repository.findById(id);
        return personOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %d", id)));
    }

    public Position find(String name) throws ServiceException {
        Optional<Position> personOptional = repository.findByNameEquals(name);
        return personOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c name = %s", name)));
    }

    public List<Position> findAll() {
        return repository.findAll();
    }

    public Position save(Position position) {
        return repository.saveAndFlush(position);
    }
}
