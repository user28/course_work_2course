package ru.yuracheglakov.courcework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.yuracheglakov.courcework.dao.PersonRepository;
import ru.yuracheglakov.courcework.domain.Person;
import ru.yuracheglakov.courcework.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    private final PersonRepository repository;

    private final ProcedureInvoker procedureInvoker;

    @Autowired
    public PersonService(PersonRepository repository, ProcedureInvoker procedureInvoker) {
        this.repository = repository;
        this.procedureInvoker = procedureInvoker;
    }


    public Person find(Long id) throws ServiceException {
        Optional<Person> personOptional = repository.findById(id);
        return personOptional.orElseThrow(() -> new ServiceException(String.format("Не найдена запись c id = %d", id)));
    }

    public List<Person> findAll() {
        return repository.findAll();
    }

    public Person create(Person person) {
        if (0 != procedureInvoker.addUser(person))
            return person;

        return null;
    }
}
