package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.*;

@Entity
@Table(name = "employer", schema = Defaults.EMPLOYERS_SCHEMA)
public class Employer extends Base {

    @Column(name = FIELD_NAME, nullable = false)
    private String name;

    @Column(name = FIELD_ADDRES, nullable = false)
    private String address;

    @Column(name = FIELD_EMAIL, nullable = false)
    private String email;

    @Column(name = FIELD_PHONE, nullable = false)
    private String phone;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_person_id")
    private Person contactPerson;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Person getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(Person contactPerson) {
        this.contactPerson = contactPerson;
    }
}
