package ru.yuracheglakov.courcework.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Base {
    static final String FIELD_NAME = "name";
    static final String FIELD_SURNAME = "surname";
    static final String FIELD_ADDRES = "addres";
    static final String FIELD_EMAIL = "email";
    static final String FIELD_PHONE = "phone";
    static final String DEGREE_FIELD = "degree";
    static final String COMMENT_FIELD = "comment";
    static final String DATE_FIELD = "date";
    static final String STATUS_FIELD = "status";
    static final String SALARY_FIELD = "salary";
    static final String SCHEDULE_FIELD = "schedule";
    static final String DESCRIPTION_FIELD = "description";
    static final String AGE_FIELD = "age";
    static final String WISHFUL_SALARY = "wishful_salary";
    static final String WISHFUL_WORK_SCHEDULE = "wishful_work_schedule";
    static final String WISHFUL_POSITION = "wishful_position";


    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
