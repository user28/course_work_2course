package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "position", schema = Defaults.DICTIONARY_SCHEMA)
public class Position extends Base {
    @Column(name = FIELD_NAME, nullable = false)
    private String name;

    @Column(name = "comment", nullable = false)
    private String comment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
