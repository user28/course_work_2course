package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "education", schema = Defaults.DICTIONARY_SCHEMA)
public class Education extends Base {
    @Column(name = DEGREE_FIELD, nullable = false)
    private String degree;

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }
}
