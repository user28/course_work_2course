package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "work_schedule", schema = Defaults.DICTIONARY_SCHEMA)
public class WorkShedule extends Base {
    @Column(name = SCHEDULE_FIELD)
    private String schedule;

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String shedule) {
        this.schedule = shedule;
    }
}
