package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.enums.VacancyStatus;
import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "vacancy", schema = Defaults.DICTIONARY_SCHEMA)
public class Vacancy extends Base {

    @ManyToOne
    private Position position;

    @ManyToOne
    private Employer employer;

    @Column(name = DATE_FIELD, nullable = false)
    private LocalDateTime date;

    @Column(name = STATUS_FIELD, nullable = false)
    private VacancyStatus status;

    @ManyToOne
    private WorkShedule workShedule;

    @Column(name = SALARY_FIELD, nullable = false)
    private Integer salary;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public VacancyStatus getStatus() {
        return status;
    }

    public void setStatus(VacancyStatus status) {
        this.status = status;
    }

    public WorkShedule getWorkShedule() {
        return workShedule;
    }

    public void setWorkShedule(WorkShedule workShedule) {
        this.workShedule = workShedule;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
