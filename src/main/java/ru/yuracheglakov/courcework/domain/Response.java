package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.enums.ResponseStatus;
import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.*;

@Entity
@Table(name = "response", schema = Defaults.DICTIONARY_SCHEMA)
public class Response {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "applicant_id")
    private Applicant applicant;

    @ManyToOne
    @JoinColumn(name = "vacancy_id")
    private Vacancy vacancy;

    @Column(name = Base.COMMENT_FIELD, nullable = false)
    private String comment;

    @Column(name = "app_status", nullable = false)
    private ResponseStatus appStatus;

    @Column(name = "employer_status", nullable = false)
    private ResponseStatus employerStatus;

    public ResponseStatus getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(ResponseStatus status) {
        this.appStatus = status;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Vacancy getVacancy() {
        return vacancy;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ResponseStatus getEmployerStatus() {
        return employerStatus;
    }

    public void setEmployerStatus(ResponseStatus employerStatus) {
        this.employerStatus = employerStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
