package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "person", schema = Defaults.DICTIONARY_SCHEMA)
public class Person extends Base {

    @Column(name = FIELD_NAME, nullable = false)
    private String name;

    @Column(name = FIELD_SURNAME, nullable = false)
    private String surName;

    @Column(name = FIELD_PHONE, nullable = false)
    private String phone;

    @Column(name = FIELD_EMAIL, nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false, unique = true)
    private String password;

    @Column(name = "role", nullable = false)
    private ru.yuracheglakov.courcework.enums.Role role;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ru.yuracheglakov.courcework.enums.Role getRole() {
        return role;
    }

    public void setRole(ru.yuracheglakov.courcework.enums.Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNameWithObriveation() {
        return String.format("%s %s.", this.surName, this.name);
    }
}
