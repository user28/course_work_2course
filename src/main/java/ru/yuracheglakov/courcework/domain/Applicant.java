package ru.yuracheglakov.courcework.domain;

import ru.yuracheglakov.courcework.util.Defaults;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "applicant", schema = Defaults.APPLICANTS_SCHEMA)
public class Applicant {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @OneToOne
    @JoinColumn(name = "person_id")
    private Person person;

    @ManyToOne
    @JoinColumn(name = "education_id")
    private Education education;

    @Column(name = Base.DESCRIPTION_FIELD, nullable = false)
    private String description;

    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @Column(name = Base.WISHFUL_POSITION)
    private String wantPosition;

    @Column(name = Base.WISHFUL_SALARY)
    private Integer wantSalary;

    @Column(name = Base.WISHFUL_WORK_SCHEDULE)
    private String wantWorkShedule;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desciption) {
        this.description = desciption;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birth_date) {
        this.birthDate = birth_date;
    }

    public String getWantPosition() {
        return wantPosition;
    }

    public void setWantPosition(String wantPosition) {
        this.wantPosition = wantPosition;
    }

    public Integer getWantSalary() {
        return wantSalary;
    }

    public void setWantSalary(Integer wantSalary) {
        this.wantSalary = wantSalary;
    }

    public String getWantWorkShedule() {
        return wantWorkShedule;
    }

    public void setWantWorkShedule(String wantWorkShedule) {
        this.wantWorkShedule = wantWorkShedule;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
