package ru.yuracheglakov.courcework.ui.panel;

import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Response;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.enums.ResponseStatus;
import ru.yuracheglakov.courcework.services.ResponseService;

import javax.swing.*;
import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ResponsePanel extends JPanel {

    private final Employer employer;
    private final ResponseService responseService;

    private static final int WIDTH = 500;
    private static final int HEIGHT = 30;
    private static final int FULL_HEIGHT = 30 * 9;
    private static final int MARGIN = 5;
    private final Font font = new Font("Arial", Font.PLAIN, 14);

    private JButton cancel;

    public ResponsePanel(Vacancy vacancy, final Employer employer, final ResponseService responseService) {

        this.employer = employer;
        this.responseService = responseService;

        List<Response> responses = responseService.find(vacancy, employer);

        for (int index = 0; index < responses.size(); index++) {
            this.add(responsePanel(responses.get(index), index), BorderLayout.SOUTH);
        }

        setBounds(5, 5, WIDTH, 400);
        setPreferredSize(new Dimension(WIDTH, 400));
        setLayout(new BorderLayout());


    }

    private JPanel responsePanel(Response response, final int index) {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);

        JLabel title = createJLabel("Отклик от соискателя: " + response.getApplicant().getPerson().getNameWithObriveation(), 0);
        JLabel appDesc = createJLabel("О себе:" + response.getApplicant().getDescription(), 1);
        JLabel birthDay = createJLabel("Дата рождения: " + response.getApplicant().getBirthDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), 2);
        JLabel education = createJLabel("Образование: " + response.getApplicant().getEducation().getDegree(), 3);
        JLabel appPhone = createJLabel("Сот. телефон соискателя: " + response.getApplicant().getPerson().getPhone(), 5);
        JLabel appStatus = createJLabel("Статус соискателя: " + response.getAppStatus().getTitle(), 6);
        JLabel employerStatus = createJLabel("Статус работодателя: " + response.getEmployerStatus().getTitle(), 7);
        JLabel appEmail = createJLabel("Электронная почта: " + response.getApplicant().getPerson().getEmail(), 4);


        JButton apply = new JButton("Подтвердить");
        apply.setBounds(MARGIN, 170, 150, HEIGHT);
        apply.addActionListener(e -> {
            response.setEmployerStatus(ResponseStatus.YES);
            responseService.save(response);
            apply.setEnabled(false);
            cancel.setEnabled(false);
        });
        apply.setFont(font);

        cancel = new JButton("Отказать");
        cancel.setBounds(MARGIN + 150 + 5, 170, 150, HEIGHT);
        cancel.addActionListener(e -> {
            response.setEmployerStatus(ResponseStatus.NO);
            responseService.save(response);
            apply.setEnabled(false);
            cancel.setEnabled(false);
        });
        cancel.setFont(font);

        JLabel result = new JLabel("Вы уже отметили данный отклик");
        result.setBounds(MARGIN, 240, 500, HEIGHT);
        result.setFont(new Font("Arial", Font.BOLD, 14));

        jPanel.setBounds(MARGIN, MARGIN + ((FULL_HEIGHT + MARGIN) * index), WIDTH, FULL_HEIGHT);
        jPanel.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));

        jPanel.add(title);
        jPanel.add(appDesc);
        jPanel.add(birthDay);
        jPanel.add(education);
        jPanel.add(appStatus);
        jPanel.add(employerStatus);
        jPanel.add(appPhone);
        jPanel.add(appEmail);
        if (response.getEmployerStatus() == ResponseStatus.NULL) {
            jPanel.add(apply);
            jPanel.add(cancel);
        } else {
            jPanel.add(result);
        }

        return jPanel;
    }

    private JLabel createJLabel(final String text, final int index) {
        JLabel jLabel = new JLabel(text);
        jLabel.setFont(font);
        jLabel.setBounds(MARGIN, MARGIN + HEIGHT * index, WIDTH, HEIGHT);

        return jLabel;
    }
}
