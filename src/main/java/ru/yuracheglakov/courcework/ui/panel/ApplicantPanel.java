package ru.yuracheglakov.courcework.ui.panel;

import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.services.ApplicantService;
import ru.yuracheglakov.courcework.services.ResponseService;
import ru.yuracheglakov.courcework.services.VacancyService;
import ru.yuracheglakov.courcework.ui.util.CreateResponseByEmployer;

import javax.swing.*;
import java.awt.*;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ApplicantPanel extends JPanel {
    private static final int WIDTH = 500;
    private static final int HEIGHT = 30;
    private static final int FULL_HEIGHT = 30 * 9;
    private static final int MARGIN = 5;
    private final Font font = new Font("Arial", Font.PLAIN, 14);

    private final ResponseService responseService;
    private final VacancyService vacancyService;
    private final Employer employer;

    public ApplicantPanel(ApplicantService applicantService, ResponseService responseService, VacancyService vacancyService, Employer employer) {
        this.responseService = responseService;
        this.vacancyService = vacancyService;
        this.employer = employer;

        List<Applicant> applicants = applicantService.findAll();

        for (int index = 0; index < applicants.size(); index++) {
            this.add(applicantPanel(applicants.get(index), index), BorderLayout.SOUTH);
        }

        setBounds(5, 5, WIDTH, 430);
        setPreferredSize(new Dimension(WIDTH, 430));
        setLayout(new BorderLayout());
    }

    private JPanel applicantPanel(Applicant applicant, int index) {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);

        JLabel name = createJLabel("Соискатель: " + applicant.getPerson().getNameWithObriveation(), 0);
        JLabel desc = createJLabel("Описание: " + applicant.getDescription(), 1);
        JLabel birthDate = createJLabel("Дата рождения: " + applicant.getBirthDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), 2);
        JLabel education = createJLabel("Образование:" + applicant.getEducation().getDegree(), 3);
        JLabel position = createJLabel("Желаемая должность: " + applicant.getWantPosition(), 4);
        JLabel salary = createJLabel("Желаемая зарплата: " + applicant.getWantSalary(), 5);
        JLabel schedule = createJLabel("График работы: " + applicant.getWantWorkShedule(), 6);

        JButton button = new JButton("Предложить вакансию");
        button.setBounds(MARGIN, HEIGHT * 7 + MARGIN, 200, 25);
        button.addActionListener(e -> new CreateResponseByEmployer(applicant, employer, responseService, vacancyService));
        button.setFont(font);

        jPanel.add(name);
        jPanel.add(desc);
        jPanel.add(birthDate);
        jPanel.add(education);
        jPanel.add(position);
        jPanel.add(salary);
        jPanel.add(schedule);
        jPanel.add(button);

        jPanel.setBounds(MARGIN, MARGIN + ((FULL_HEIGHT + MARGIN) * index), WIDTH, FULL_HEIGHT);
        jPanel.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));

        return jPanel;
    }

    private JLabel createJLabel(final String text, final int index) {
        JLabel jLabel = new JLabel(text);
        jLabel.setFont(font);
        jLabel.setBounds(MARGIN, MARGIN + HEIGHT * index, WIDTH, HEIGHT);

        return jLabel;
    }
}
