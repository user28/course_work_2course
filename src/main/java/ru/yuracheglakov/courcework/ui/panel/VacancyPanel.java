package ru.yuracheglakov.courcework.ui.panel;

import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Response;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.enums.VacancyStatus;
import ru.yuracheglakov.courcework.exceptions.ServiceException;
import ru.yuracheglakov.courcework.services.ResponseService;
import ru.yuracheglakov.courcework.services.VacancyService;
import ru.yuracheglakov.courcework.ui.MainFrame;
import ru.yuracheglakov.courcework.ui.util.CreateResponse;
import ru.yuracheglakov.courcework.ui.util.EmployerApplicants;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class VacancyPanel extends JPanel {

    private static final int WIDTH = 765;
    private static final int HEIGHT = 30;
    private static final int FULL_HEIGHT = 35 * 8;
    private static final int MARGIN = 5;

    private final VacancyService vacancyService;
    private final Employer employer;
    private final ResponseService responseService;
    private final Applicant applicant;
    private static List<JButton> buttons = new ArrayList<>();
    private static List<JLabel> errorsList = new ArrayList<>();

    public VacancyPanel(VacancyService vacancyService, Employer employer, ResponseService responseService) {
        this.vacancyService = vacancyService;
        this.employer = employer;
        this.responseService = responseService;
        this.applicant = MainFrame.getApplicant();

        List<Vacancy> vacancies = null == employer ? vacancyService.findAll(VacancyStatus.ACTIVE) : vacancyService.findAll(employer);

        if (null == employer)
            for (int index = 0; index < vacancies.size(); index++) {
                if (null != applicant) {
                    this.add(createVacancyPanel(vacancies.get(index), index), BorderLayout.SOUTH);
                }
            }
        else {
            for (int index = 0; index < vacancies.size(); index++) {
                this.add(createEmployerVacancyPanel(vacancies.get(index), index), BorderLayout.SOUTH);
            }
        }

        setBounds(5, 5, WIDTH, 400);
        setPreferredSize(new Dimension(WIDTH, (FULL_HEIGHT * vacancies.size()) + (MARGIN * vacancies.size()) + 20));
        setLayout(new BorderLayout(0, 0));

        setVisible(true);
    }

    private JPanel createVacancyPanel(Vacancy vacancy, int index) {

        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jPanel.setVisible(true);

        Response response = null;
        try {
            response = responseService.find(vacancy, applicant);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        final LocalDateTime localDateTime = LocalDateTime.now();

        localDateTime.toLocalDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));

        JLabel vacancyCreateDate = createJLabel("Дата создания вакансии: " + vacancy.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        vacancyCreateDate.setBounds(MARGIN, MARGIN, WIDTH, HEIGHT);

        JLabel vacancyName = createJLabel("Должность: " + vacancy.getPosition().getName());
        vacancyName.setBounds(MARGIN, HEIGHT * 2, WIDTH, HEIGHT);

        JLabel vacancySalary = createJLabel("Зарплата: " + vacancy.getSalary().toString() + "р");
        vacancySalary.setBounds(MARGIN, HEIGHT * 3, WIDTH, HEIGHT);

        JLabel vacancyStatus = createJLabel("Статус: " + vacancy.getStatus().getTitle());
        vacancyStatus.setBounds(MARGIN, HEIGHT * 4, WIDTH, HEIGHT);

        JLabel employerName = createJLabel("Работодатель: " + vacancy.getEmployer().getName());
        employerName.setBounds(MARGIN, HEIGHT * 5, WIDTH, HEIGHT);

        JLabel employerPhoneAndEmail = createJLabel("Рабочий телефон: " + vacancy.getEmployer().getPhone() + ", электронная почта: " + vacancy.getEmployer().getEmail());
        employerPhoneAndEmail.setBounds(MARGIN, HEIGHT * 6, WIDTH, HEIGHT);

        JLabel employerAddress = createJLabel("Адрес: " + vacancy.getEmployer().getAddress());
        employerAddress.setBounds(MARGIN, HEIGHT * 7, WIDTH, HEIGHT);

        JLabel applicantError = new JLabel("");
        applicantError.setBounds(150, HEIGHT * 8, WIDTH, HEIGHT);
        applicantError.setFont((new Font("Arial", Font.BOLD, 14)));
        errorsList.add(applicantError);

        JButton createResponse = new JButton("Откликнутся");
        if (null == applicant) {
            createResponse.setEnabled(false);
            applicantError.setText("Пожалуйста, заполните данные соискателя");
        }

        if (null != response) {
            createResponse.setEnabled(false);
            applicantError.setText("Вы уже оставили заявку на эту вакансию.");
        }
        createResponse.setBounds(600, HEIGHT * 8, 140, HEIGHT);
        createResponse.addActionListener(e -> new CreateResponse(vacancy, applicant, responseService));
        createResponse.setFont((new Font("Arial", Font.PLAIN, 14)));
        if (null == response)
            buttons.add(createResponse);

        jPanel.setBounds(MARGIN, MARGIN + ((FULL_HEIGHT + MARGIN) * index), WIDTH, FULL_HEIGHT);
        jPanel.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));

        jPanel.add(vacancyCreateDate);
        jPanel.add(vacancyName);
        jPanel.add(vacancySalary);
        jPanel.add(vacancyStatus);
        jPanel.add(employerName);
        jPanel.add(employerPhoneAndEmail);
        jPanel.add(employerAddress);
        jPanel.add(createResponse);
        jPanel.add(applicantError);

        return jPanel;
    }

    private JPanel createEmployerVacancyPanel(Vacancy vacancy, int index) {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jPanel.setVisible(true);

        Integer count = responseService.count(vacancy);

        JLabel vacancyCreateDate = createJLabel("Дата создания вакансии: " + vacancy.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        vacancyCreateDate.setBounds(MARGIN, MARGIN, WIDTH, HEIGHT);

        JLabel vacancyName = createJLabel("Должность: " + vacancy.getPosition().getName());
        vacancyName.setBounds(MARGIN, HEIGHT * 2, WIDTH, HEIGHT);

        JLabel vacancySalary = createJLabel("Зарплата: " + vacancy.getSalary().toString() + "р");
        vacancySalary.setBounds(MARGIN, HEIGHT * 3, WIDTH, HEIGHT);

        JLabel vacancyStatus = createJLabel("Статус: " + vacancy.getStatus().getTitle());
        vacancyStatus.setBounds(MARGIN, HEIGHT * 4, WIDTH, HEIGHT);

        JLabel vacancyResponses = createJLabel("Количество откликов: " + count);
        vacancyResponses.setBounds(MARGIN, HEIGHT * 5, WIDTH, HEIGHT);

        if (1 <= count) {
            JButton seeResponses = new JButton("Просмотреть");
            seeResponses.setBounds(175, HEIGHT * 5, 140, HEIGHT);
            seeResponses.setFont((new Font("Arial", Font.PLAIN, 14)));
            seeResponses.addActionListener(e -> new EmployerApplicants(vacancy, employer, responseService));
            jPanel.add(seeResponses);
        }

        jPanel.setBounds(MARGIN, MARGIN + ((FULL_HEIGHT + MARGIN) * index), WIDTH, FULL_HEIGHT);
        jPanel.setBorder(BorderFactory.createLineBorder(Color.darkGray, 1));

        jPanel.add(vacancyCreateDate);
        jPanel.add(vacancyResponses);
        jPanel.add(vacancyName);
        jPanel.add(vacancySalary);
        jPanel.add(vacancyStatus);

        return jPanel;
    }

    public static List<JButton> getButtons() {
        return buttons;
    }

    public static List<JLabel> getErrorsList() {
        return errorsList;
    }

    private JLabel createJLabel(String text) {
        JLabel jLabel = new JLabel(text);
        jLabel.setFont((new Font("Arial", Font.PLAIN, 14)));

        return jLabel;
    }
}
