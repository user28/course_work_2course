package ru.yuracheglakov.courcework.ui;

import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Person;
import ru.yuracheglakov.courcework.exceptions.ServiceException;
import ru.yuracheglakov.courcework.services.*;
import ru.yuracheglakov.courcework.ui.panel.VacancyPanel;
import ru.yuracheglakov.courcework.ui.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainFrame extends JFrame {

    private static Person person = null;
    private static Employer employer = null;
    private static Applicant applicant = null;

    public MainFrame(PersonService personService, Long personId, VacancyService vacancyService, WorkSheduleService workSheduleService, PositionService positionService, EmployerService employerService, ResponseService responseService, ApplicantService applicantService, EducationService educationService) throws ServiceException {
        setResizable(false);
        setVisible(true);
        setSize(new Dimension(1280, 1020));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        person = personService.find(personId);
        employer = employerService.findByContantPerson(personId);
        try {
            applicant = applicantService.find(person);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setTitle(String.format("Бюро трудоустройств: %s %s", person.getName(), person.getSurName()));

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JMenuBar menuBar = new JMenuBar();
        menuBar.setBounds(0, 0, getWidth(), 21);
        contentPanel.add(menuBar);

        JMenu menu = new JMenu("Файл");
        menuBar.add(menu);

        JMenuItem menuItem_1 = new JMenuItem("Закрыть");
        menuItem_1.addActionListener(arg0 -> System.exit(0));
        menu.add(menuItem_1);

        if (person.getRole().getId() == 2 || person.getRole().isManager()) {
            JMenu menu_1 = new JMenu("Компания");
            menuBar.add(menu_1);

            JMenuItem menuItem_8 = new JMenuItem("Изменить данные");
            menuItem_8.addActionListener(e -> new ChangeEmployerData(employer, employerService));
            menu_1.add(menuItem_8);

            JMenu menu_2 = new JMenu("Отклики");
            menuBar.add(menu_2);

            JMenuItem menuItem_9 = new JMenuItem("Просмотреть отклики");
            menuItem_9.addActionListener(e -> new EmployerApplicants(null, employer, responseService));
            menu_2.add(menuItem_9);
        }

        JMenu menu_2 = new JMenu("Соискатели");
        menuBar.add(menu_2);

        JScrollPane vacanciesPane = new JScrollPane(new VacancyPanel(vacancyService, employer, responseService));

        vacanciesPane.setLocation(5, 55);
        vacanciesPane.setSize(new Dimension(getWidth() - 15, getHeight() - 20));
        vacanciesPane.getVerticalScrollBar().setUnitIncrement(16);
        contentPanel.add(vacanciesPane);

        JMenuItem menuItem = new JMenuItem("Добавить/изменить данные о себе");
        menuItem.addActionListener(e -> new ChangeApplicantData(applicantService, person, educationService, this, applicant));
        if (person.getRole().getId() == 1)
            menu_2.add(menuItem);

        JMenuItem menuItem_2 = new JMenuItem("Открыть список соискателей");
        menuItem_2.addActionListener(e -> new ApplicantsFrame(applicantService, responseService, vacancyService, employer));
        if (person.getRole().getId() == 2)
            menu_2.add(menuItem_2);


        if (person.getRole().getId() == 2 || person.getRole().isManager()) {
            JMenu menu_3 = new JMenu("Вакансии");
            menuBar.add(menu_3);
            JMenuItem menuItem_3 = new JMenuItem("Создать вакансию");
            menuItem_3.addActionListener(e -> new CreateVacancyFrame(vacancyService, positionService, workSheduleService, employerService.findByContantPerson(personId)));
            menu_3.add(menuItem_3);
        }

        JLabel labelVacancy = null == employer ? new JLabel("Список открытых вакансий:") : new JLabel("Список Ваших вакансий:");
        labelVacancy.setFont(new Font("Arial", Font.PLAIN, 14));
        labelVacancy.setBounds(5, 28, 207, 14);
        contentPanel.add(labelVacancy);
    }

    private static void addPopup(Component component, final JPopupMenu popup) {
        component.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showMenu(e);
                }
            }

            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showMenu(e);
                }
            }

            private void showMenu(MouseEvent e) {
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        });
    }


    public static Person getPerson() {
        return person;
    }

    public static Employer getEmployer() {
        return employer;
    }

    public static Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        MainFrame.applicant = applicant;
    }
}
