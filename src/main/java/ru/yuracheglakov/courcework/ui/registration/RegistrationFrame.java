package ru.yuracheglakov.courcework.ui.registration;

import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Person;
import ru.yuracheglakov.courcework.exceptions.ServiceException;
import ru.yuracheglakov.courcework.services.*;
import ru.yuracheglakov.courcework.ui.MainFrame;
import ru.yuracheglakov.courcework.util.Errors;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static ru.yuracheglakov.courcework.util.Errors.EMPLOYER_EMAIL_ERROR;

public class RegistrationFrame extends JFrame {
    private final JPasswordField password;
    private final JPasswordField appPassword;

    private final JTextField appName;
    private final JTextField appSurName;
    private final JTextField appEmail;
    private final JTextField appPhone;
    private final JTextField name;
    private final JTextField surName;
    private final JTextField email;
    private final JTextField phone;
    private final JTextField employerName;
    private final JTextField employerAddress;
    private final JTextField employerEmail;
    private final JTextField employerPhone;
    private PersonService personService;
    private JLabel employerNameError;
    private JLabel employerAddressError;
    private JLabel employerEmailError;
    private JLabel employerPhonError;
    private JLabel personNameError;
    private JLabel personSurNameError;
    private JLabel personEmailError;
    private JLabel personPhoneError;
    private JLabel appNameError;
    private JLabel appSurNameError;
    private JLabel appEmailError;
    private JLabel appPhoneError;
    private JPanel applicantPanel;
    private JPanel employerPanel;

    public RegistrationFrame(ProcedureInvoker procedureInvoker, PersonService personService,
                             EmployerService employerService, VacancyService vacancyService, WorkSheduleService workSheduleService,
                             PositionService positionService, ResponseService responseService, ApplicantService applicantService, EducationService educationService) {
        this.personService = personService;

        setResizable(false);
        setSize(new Dimension(800, 600));
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel contentPane = new JPanel();
        getContentPane().add(contentPane, BorderLayout.CENTER);
        contentPane.setLayout(null);

        JPanel checkPanel = new JPanel();
        checkPanel.setBounds(0, 0, 784, 565);
        contentPane.add(checkPanel);
        checkPanel.setLayout(null);

        JLabel label = new JLabel("Вы работодатель или соискатель?");
        label.setFont(new Font("Arial", Font.PLAIN, 14));
        label.setBounds(241, 175, 378, 14);
        checkPanel.add(label);

        JRadioButton employerCheck = new JRadioButton("Работодатель");
        employerCheck.setFont(new Font("Arial", Font.PLAIN, 14));
        employerCheck.setBounds(209, 196, 188, 23);
        employerCheck.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPanel.setVisible(false);
                employerPanel.setVisible(true);
            }
        });
        checkPanel.add(employerCheck);

        JRadioButton applicantCheck = new JRadioButton("Соискатель");
        applicantCheck.setFont(new Font("Arial", Font.PLAIN, 14));
        applicantCheck.setBounds(436, 196, 188, 23);
        applicantCheck.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPanel.setVisible(false);
                applicantPanel.setVisible(true);
            }
        });
        checkPanel.add(applicantCheck);

        applicantPanel = new JPanel();
        applicantPanel.setBounds(0, 0, 794, 575);
        contentPane.add(applicantPanel);
        applicantPanel.setLayout(null);

        JLabel label_3 = new JLabel("Пожалуйста, введите Ваши данные:");
        label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
        label_3.setBounds(10, 11, 761, 24);
        applicantPanel.add(label_3);

        appName = new JTextField();
        appName.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        appName.setBounds(255, 86, 245, 25);
        applicantPanel.add(appName);

        appSurName = new JTextField();
        appSurName.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        appSurName.setBounds(255, 122, 245, 25);
        applicantPanel.add(appSurName);

        appEmail = new JTextField();
        appEmail.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        appEmail.setBounds(255, 158, 245, 25);
        applicantPanel.add(appEmail);

        appPhone = new JTextField();
        appPhone.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        appPhone.setBounds(255, 194, 245, 25);
        applicantPanel.add(appPhone);

        appPassword = new JPasswordField();
        appPassword.setBorder(UIManager.getBorder("TextArea.border"));
        appPassword.setBounds(255, 230, 245, 25);
        applicantPanel.add(appPassword);

        JButton button_1 = new JButton("Регистрация");
        button_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                List<Integer> errors = checkApp();

                appName.setBackground(Color.WHITE);
                appSurName.setBackground(Color.WHITE);
                appPhone.setBackground(Color.WHITE);
                appEmail.setBackground(Color.WHITE);

                if (0 == errors.size()) {

                    Long userId = procedureInvoker
                            .addUser(fillPerson(ru.yuracheglakov.courcework.enums.Role.APPLICANT));
                    if (null != userId) {
                        setVisible(false);
                        try {
                            new MainFrame(personService, userId, vacancyService, workSheduleService, positionService,
                                    employerService, responseService, applicantService, educationService);
                        } catch (ServiceException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                } else {
                    for (Integer error : errors) {
                        switch (error) {
                            case 4:
                                appName.setBackground(Color.ORANGE);
                                appNameError.setText("Только русские буквы.");
                                break;
//                                SURnAME ERROR
                            case 5:
                                personSurNameError.setText("Только русские буквы.");
                                appSurNameError.setBackground(Color.ORANGE);
                                break;
//                                PHONE ERROR
                            case 6:
                                appPhoneError.setText("Неверный сот. телефон.");
                                appPhone.setBackground(Color.ORANGE);
                                break;
//                                EMAILERROR
                            case 7:
                                appEmail.setBackground(Color.ORANGE);
                                appEmailError.setText("Неверный эл. адрес.");
                                break;
                        }
                    }
                }
            }
        });
        button_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        button_1.setBounds(315, 266, 127, 35);
        applicantPanel.add(button_1);

        JLabel label_4 = new JLabel("Имя:");
        label_4.setFont(new Font("Arial", Font.PLAIN, 14));
        label_4.setBounds(85, 92, 128, 14);
        applicantPanel.add(label_4);

        JLabel label_5 = new JLabel("Фамилия:");
        label_5.setFont(new Font("Arial", Font.PLAIN, 14));
        label_5.setBounds(85, 128, 128, 14);
        applicantPanel.add(label_5);

        JLabel label_6 = new JLabel("Электронная почта:");
        label_6.setFont(new Font("Arial", Font.PLAIN, 14));
        label_6.setBounds(85, 165, 160, 14);
        applicantPanel.add(label_6);

        JLabel label_7 = new JLabel("Сот. телефон");
        label_7.setFont(new Font("Arial", Font.PLAIN, 14));
        label_7.setBounds(85, 200, 128, 14);
        applicantPanel.add(label_7);

        JLabel label_8 = new JLabel("Пароль:");
        label_8.setFont(new Font("Arial", Font.PLAIN, 14));
        label_8.setBounds(85, 234, 128, 14);
        applicantPanel.add(label_8);

        appNameError = new JLabel("");
        appNameError.setFont(new Font("Arial", Font.PLAIN, 14));
        appNameError.setBounds(510, 89, 261, 22);
        applicantPanel.add(appNameError);

        appSurNameError = new JLabel("");
        appSurNameError.setFont(new Font("Arial", Font.PLAIN, 14));
        appSurNameError.setBounds(510, 122, 261, 22);
        applicantPanel.add(appSurNameError);

        appEmailError = new JLabel("");
        appEmailError.setFont(new Font("Arial", Font.PLAIN, 14));
        appEmailError.setBounds(510, 161, 261, 22);
        applicantPanel.add(appEmailError);

        appPhoneError = new JLabel("");
        appPhoneError.setFont(new Font("Arial", Font.PLAIN, 14));
        appPhoneError.setBounds(510, 197, 261, 22);
        applicantPanel.add(appPhoneError);
        applicantPanel.setVisible(false);

        employerPanel = new JPanel();
        employerPanel.setBounds(0, 0, 784, 565);
        contentPane.add(employerPanel);
        employerPanel.setLayout(null);

        JLabel label_1 = new JLabel("Пожалуйста, введите Ваши данные:");
        label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
        label_1.setBounds(268, 39, 298, 24);
        employerPanel.add(label_1);

        name = new JTextField();
        name.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        name.setBounds(268, 74, 245, 25);
        employerPanel.add(name);

        surName = new JTextField();
        surName.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        surName.setBounds(268, 110, 245, 25);
        employerPanel.add(surName);

        email = new JTextField();
        email.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        email.setBounds(268, 146, 245, 25);
        employerPanel.add(email);

        phone = new JTextField();
        phone.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        phone.setBounds(268, 182, 245, 25);
        employerPanel.add(phone);

        password = new JPasswordField();
        password.setBorder(UIManager.getBorder("TextArea.border"));
        password.setBounds(268, 218, 245, 25);
        employerPanel.add(password);

        JLabel label_2 = new JLabel("Пожалуйста, введите данные Вашей организации:");
        label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
        label_2.setBounds(224, 301, 342, 24);
        employerPanel.add(label_2);

        employerName = new JTextField();
        employerName.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        employerName.setBounds(268, 336, 245, 25);
        employerPanel.add(employerName);

        employerAddress = new JTextField();
        employerAddress.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        employerAddress.setBounds(268, 372, 245, 25);
        employerPanel.add(employerAddress);

        employerEmail = new JTextField();
        employerEmail.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        employerEmail.setBounds(268, 408, 245, 25);
        employerPanel.add(employerEmail);

        employerPhone = new JTextField();
        employerPhone.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        employerPhone.setBounds(268, 444, 245, 25);
        employerPanel.add(employerPhone);

        JButton button = new JButton("Регистрация");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                List<Integer> errors = checkEmployer();
                errors.addAll(checkPerson());

                employerEmail.setBackground(Color.WHITE);
                employerPhone.setBackground(Color.WHITE);
                name.setBackground(Color.WHITE);
                surName.setBackground(Color.WHITE);
                phone.setBackground(Color.WHITE);
                email.setBackground(Color.WHITE);

                if (0 == errors.size()) {

                    Long userId = procedureInvoker.addUser(fillPerson(ru.yuracheglakov.courcework.enums.Role.EMPLOYER));

                    if (null != userId) {
                        setVisible(false);
                        try {
                            employerService.create(fillEmployer(userId));
                            new MainFrame(personService, userId, vacancyService, workSheduleService, positionService,
                                    employerService, responseService, applicantService, educationService);
                        } catch (ServiceException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                } else {
                    for (Integer error : errors) {
                        switch (error) {
                            case 2:
                                employerEmailError.setText("Неверный эл. адрес.");
                                employerEmail.setBackground(Color.ORANGE);
                                break;
                            case 1:
                                employerPhonError.setText("Неверный телефон.");
                                employerPhone.setBackground(Color.ORANGE);
                                break;
//                                NAME ERORR
                            case 4:
                                name.setBackground(Color.ORANGE);
                                personNameError.setText("Только русские буквы.");
                                break;
//                                SURnAME ERROR
                            case 5:
                                personSurNameError.setText("Только русские буквы.");
                                surName.setBackground(Color.ORANGE);
                                break;
//                                PHONE ERROR
                            case 6:
                                personPhoneError.setText("Неверный сот. телефон.");
                                phone.setBackground(Color.ORANGE);
                                break;
//                                EMAILERROR
                            case 7:
                                email.setBackground(Color.ORANGE);
                                personEmailError.setText("Почта указана неверно!");
                                break;
                        }
                    }
                }
            }
        });
        button.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        button.setBounds(328, 480, 127, 35);
        employerPanel.add(button);

        JLabel label_9 = new JLabel("Имя:");
        label_9.setFont(new Font("Arial", Font.PLAIN, 14));
        label_9.setBounds(114, 80, 114, 14);
        employerPanel.add(label_9);

        JLabel label_10 = new JLabel("Фамилия:");
        label_10.setFont(new Font("Arial", Font.PLAIN, 14));
        label_10.setBounds(114, 117, 114, 14);
        employerPanel.add(label_10);

        JLabel label_11 = new JLabel("Электронная почта:");
        label_11.setFont(new Font("Arial", Font.PLAIN, 14));
        label_11.setBounds(114, 153, 144, 14);
        employerPanel.add(label_11);

        JLabel label_12 = new JLabel("Сот. телефон:");
        label_12.setFont(new Font("Arial", Font.PLAIN, 14));
        label_12.setBounds(114, 189, 114, 14);
        employerPanel.add(label_12);

        JLabel label_13 = new JLabel("Пароль:");
        label_13.setFont(new Font("Arial", Font.PLAIN, 14));
        label_13.setBounds(114, 223, 114, 14);
        employerPanel.add(label_13);

        JLabel label_14 = new JLabel("Название организации:");
        label_14.setFont(new Font("Arial", Font.PLAIN, 14));
        label_14.setBounds(90, 342, 154, 14);
        employerPanel.add(label_14);

        JLabel label_15 = new JLabel("Адрес организации:");
        label_15.setFont(new Font("Arial", Font.PLAIN, 14));
        label_15.setBounds(90, 379, 154, 14);
        employerPanel.add(label_15);

        JLabel label_16 = new JLabel("Электронная почта:");
        label_16.setFont(new Font("Arial", Font.PLAIN, 14));
        label_16.setBounds(90, 415, 154, 14);
        employerPanel.add(label_16);

        JLabel label_17 = new JLabel("Рабочий телефон:");
        label_17.setFont(new Font("Arial", Font.PLAIN, 14));
        label_17.setBounds(90, 451, 154, 14);
        employerPanel.add(label_17);

        employerNameError = new JLabel("");
        employerNameError.setFont(new Font("Arial", Font.PLAIN, 14));
        employerNameError.setBounds(523, 336, 251, 22);
        employerPanel.add(employerNameError);

        employerAddressError = new JLabel("");
        employerAddressError.setFont(new Font("Arial", Font.PLAIN, 14));
        employerAddressError.setBounds(523, 372, 251, 22);
        employerPanel.add(employerAddressError);

        employerEmailError = new JLabel("");
        employerEmailError.setFont(new Font("Arial", Font.PLAIN, 14));
        employerEmailError.setBounds(523, 408, 251, 22);
        employerPanel.add(employerEmailError);

        employerPhonError = new JLabel("");
        employerPhonError.setFont(new Font("Arial", Font.PLAIN, 14));
        employerPhonError.setBounds(523, 444, 251, 22);
        employerPanel.add(employerPhonError);

        personNameError = new JLabel("");
        personNameError.setFont(new Font("Arial", Font.PLAIN, 14));
        personNameError.setBounds(523, 74, 251, 22);
        employerPanel.add(personNameError);

        personSurNameError = new JLabel("");
        personSurNameError.setFont(new Font("Arial", Font.PLAIN, 14));
        personSurNameError.setBounds(523, 110, 251, 22);
        employerPanel.add(personSurNameError);

        personEmailError = new JLabel("");
        personEmailError.setFont(new Font("Arial", Font.PLAIN, 14));
        personEmailError.setBounds(523, 146, 251, 22);
        employerPanel.add(personEmailError);

        personPhoneError = new JLabel("");
        personPhoneError.setFont(new Font("Arial", Font.PLAIN, 14));
        personPhoneError.setBounds(523, 182, 251, 22);
        employerPanel.add(personPhoneError);

        employerPanel.setVisible(false);

    }

    private Employer fillEmployer(Long userId) throws ServiceException {
        Employer employer = new Employer();

        Person person = personService.find(userId);

        employer.setAddress(employerAddress.getText());
        employer.setContactPerson(person);
        employer.setEmail(employerEmail.getText());
        employer.setName(employerName.getText());
        employer.setPhone(employerPhone.getText());

        return employer;
    }

    private Person fillPerson(ru.yuracheglakov.courcework.enums.Role role) {
        Person person = new Person();

        if (role.getId() == 1) {
            person.setEmail(appEmail.getText());
            person.setName(appName.getText());
            person.setPhone(appPhone.getText());
            person.setSurName(appSurName.getText());
            person.setPassword(new String(appPassword.getPassword()));
            person.setRole(role);
        } else {
            person.setEmail(email.getText());
            person.setName(name.getText());
            person.setPhone(phone.getText());
            person.setSurName(surName.getText());
            person.setRole(role);
            person.setPassword(new String(password.getPassword()));
        }

        return person;
    }

    private List<Integer> checkEmployer() {

        List<Integer> errors = new ArrayList<>();

        if (!Errors.checkPhoneNumber(employerPhone.getText())) {
            errors.add(Errors.EMPLOYER_PHONE_ERROR);
        }

        if (!Errors.checkEmail(employerEmail.getText())) {
            errors.add(EMPLOYER_EMAIL_ERROR);
        }

        return errors;
    }

    private List<Integer> checkPerson() {

        List<Integer> errors = new ArrayList<>();

        if (!Errors.checkEmail(email.getText())) {
            errors.add(Errors.PERSON_EMAIL_ERROR);
        }

        if (!Errors.checkPhoneNumber(phone.getText())) {
            errors.add(Errors.PERSON_PHONE_ERROR);
        }

        if (!Errors.checkLatin(name.getText())) {
            errors.add(Errors.PERSON_NAME_ERROR);
        }

        if (!Errors.checkLatin(surName.getText())) {
            errors.add(Errors.PERSON_SURNAME_ERROR);
        }

        return errors;
    }

    private List<Integer> checkApp() {
        List<Integer> errors = new ArrayList<>();

        if (Errors.checkEmail(appEmail.getText())) {
            errors.add(Errors.PERSON_EMAIL_ERROR);
        }

        if (!Errors.checkPhoneNumber(appPhone.getText())) {
            errors.add(Errors.PERSON_PHONE_ERROR);
        }

        if (!Errors.checkLatin(appName.getText())) {
            errors.add(Errors.PERSON_NAME_ERROR);
        }

        if (!Errors.checkLatin(appSurName.getText())) {
            errors.add(Errors.PERSON_SURNAME_ERROR);
        }

        return errors;
    }
}
