package ru.yuracheglakov.courcework.ui.util;

import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Education;
import ru.yuracheglakov.courcework.domain.Person;
import ru.yuracheglakov.courcework.exceptions.ServiceException;
import ru.yuracheglakov.courcework.services.ApplicantService;
import ru.yuracheglakov.courcework.services.EducationService;
import ru.yuracheglakov.courcework.ui.MainFrame;
import ru.yuracheglakov.courcework.ui.panel.VacancyPanel;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ChangeApplicantData extends JFrame {
    private JTextField salaryField;
    private JTextField workShedule;
    private JTextField position;
    private JTextField birthDate;
    private JTextArea textArea;
    private JScrollPane education;
    private EducationService educationService;

    public ChangeApplicantData(ApplicantService applicantService, Person person, EducationService educationService, MainFrame mainFrame, Applicant applicant) {
        this.educationService = educationService;

        setTitle("Данные соискателя");
        setVisible(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        setSize(new Dimension(600, 400));
        setBounds(0, 0, 600, 400);

        JLabel result = new JLabel("");
        result.setFont(new Font("Arial", Font.PLAIN, 14));
        result.setBounds(10, 385, 764, 14);
        contentPanel.add(result);

        JLabel label_3 = new JLabel("О себе:");
        label_3.setFont(new Font("Arial", Font.PLAIN, 14));
        label_3.setBounds(10, 11, 68, 14);
        contentPanel.add(label_3);

        textArea = new JTextArea();
        if (null != applicant) {
            textArea.setText(applicant.getDescription());
        }
        textArea.setBounds(65, 12, 709, 132);
        contentPanel.add(textArea);

        JLabel label = new JLabel("Желаемая зарплата:");
        label.setFont(new Font("Arial", Font.PLAIN, 14));
        label.setBounds(10, 173, 140, 14);
        contentPanel.add(label);

        JLabel label_1 = new JLabel("Желаемый рабочий график:");
        label_1.setFont(new Font("Arial", Font.PLAIN, 14));
        label_1.setBounds(10, 199, 200, 14);
        contentPanel.add(label_1);

        JLabel label_2 = new JLabel("Желаемая должность:");
        label_2.setFont(new Font("Arial", Font.PLAIN, 14));
        label_2.setBounds(10, 228, 200, 14);
        contentPanel.add(label_2);

        JLabel label_4 = new JLabel("Образование:");
        label_4.setFont(new Font("Arial", Font.PLAIN, 14));
        label_4.setBounds(10, 257, 200, 14);
        contentPanel.add(label_4);

        JLabel label_5 = new JLabel("Дата рождения");
        label_5.setFont(new Font("Arial", Font.PLAIN, 14));
        label_5.setBounds(10, 288, 200, 14);
        contentPanel.add(label_5);

        salaryField = new JTextField();
        if (null != applicant) {
            salaryField.setText(applicant.getWantSalary().toString());
        }
        salaryField.setBounds(212, 168, 200, 20);
        contentPanel.add(salaryField);
        salaryField.setColumns(10);

        workShedule = new JTextField();
        if (null != applicant) {
            workShedule.setText(applicant.getWantWorkShedule());
        }
        workShedule.setColumns(10);
        workShedule.setBounds(212, 197, 200, 20);
        contentPanel.add(workShedule);

        position = new JTextField();
        if (null != applicant) {
            position.setText(applicant.getWantPosition());
        }
        position.setColumns(10);
        position.setBounds(212, 226, 200, 20);
        contentPanel.add(position);

        birthDate = new JTextField();
        if (null != applicant) {
            birthDate.setText(applicant.getBirthDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        }
        birthDate.setColumns(10);
        birthDate.setBounds(212, 286, 200, 20);
        contentPanel.add(birthDate);


        JList educations = fillJListEducation(educationService);
        educations.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        education = new JScrollPane(educations);
        education.setBounds(212, 257, 200, 20);
        education.setPreferredSize(new Dimension(200, 20));
        contentPanel.add(education);

        JButton btnNewButton = new JButton("Отмена");
        btnNewButton.addActionListener(e -> setVisible(false));
        btnNewButton.setFont(new Font("Arial", Font.PLAIN, 14));
        btnNewButton.setBounds(143, 352, 89, 23);
        contentPanel.add(btnNewButton);

        JButton button = new JButton("Сохранить");
        button.addActionListener(e -> {
            try {
                Applicant newApplicant = applicantService.save(fillApplicant(applicant, person, educations));

                if (null != newApplicant) {
                    result.setText("Данные успешно сохранены");
                    mainFrame.setApplicant(newApplicant);
                    btnNewButton.setEnabled(false);

                    button.setText("Закрыть");
                    button.addActionListener(e1 -> {
                        setVisible(false);

                        for (JButton jButton : VacancyPanel.getButtons()) {
                            jButton.setEnabled(true);
                        }
                        for (JLabel jLabel : VacancyPanel.getErrorsList()) {
                            jLabel.setText("");
                        }
                    });
                } else {
                    result.setText("Произошла ошибка. Попробуйте позже или обратитесь к разработчику");
                }
            } catch (ServiceException e1) {
                e1.printStackTrace();
            }
        });
        button.setFont(new Font("Arial", Font.PLAIN, 14));
        button.setBounds(10, 351, 123, 23);
        contentPanel.add(button);

    }

    private Applicant fillApplicant(Applicant applicant, Person person, JList educations) throws ServiceException {
        Applicant newApplicant = new Applicant();

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        final LocalDate localDate = LocalDate.parse(birthDate.getText(), formatter);

        if (null != applicant) {
            newApplicant.setId(applicant.getId());
        }
        newApplicant.setBirthDate(localDate);
        newApplicant.setDescription(textArea.getText());
        newApplicant.setEducation(educationService.find(educations.getSelectedValue().toString()));
        newApplicant.setPerson(person);
        newApplicant.setWantPosition(position.getText());
        newApplicant.setWantSalary(Integer.parseInt(salaryField.getText()));
        newApplicant.setWantWorkShedule(workShedule.getText());


        return newApplicant;
    }

    private JList<String> fillJListEducation(EducationService educationService) {

        List<Education> educationList = educationService.findAll();

        DefaultListModel<String> defaultListModel = new DefaultListModel<>();

        for (int index = 0; index < educationList.size(); index++) {
            defaultListModel.add(index, educationList.get(index).getDegree());
        }

        return new JList<>(defaultListModel);
    }
}
