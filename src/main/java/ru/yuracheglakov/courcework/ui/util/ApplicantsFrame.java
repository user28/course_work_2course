package ru.yuracheglakov.courcework.ui.util;

import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.services.ApplicantService;
import ru.yuracheglakov.courcework.services.ResponseService;
import ru.yuracheglakov.courcework.services.VacancyService;
import ru.yuracheglakov.courcework.ui.panel.ApplicantPanel;

import javax.swing.*;
import java.awt.*;

public class ApplicantsFrame extends JFrame {

    public ApplicantsFrame(ApplicantService applicantService, ResponseService responseService, VacancyService vacancyService, Employer employer) {

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        setVisible(true);
        setSize(new Dimension(600, 800));
        setTitle("Соискатели");

        JButton button = new JButton("Закрыть");
        button.addActionListener(e -> setVisible(false));
        button.setFont(new Font("Arial", Font.PLAIN, 14));
        button.setBounds(450, 700, 100, 25);
        contentPanel.add(button);

        JScrollPane responsePanel = new JScrollPane(new ApplicantPanel(applicantService, responseService, vacancyService, employer));
        responsePanel.setBounds(10, 11, 510, 600);
        contentPanel.add(responsePanel);

    }
}
