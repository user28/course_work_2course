package ru.yuracheglakov.courcework.ui.util;

import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.services.EmployerService;
import ru.yuracheglakov.courcework.util.Errors;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static ru.yuracheglakov.courcework.util.Errors.EMPLOYER_EMAIL_ERROR;

public class ChangeEmployerData extends JFrame {
    private JTextField nameField;
    private JTextField phoneField;
    private JTextField addressField;
    private JTextField emailField;
    private JLabel employerPhoneError;
    private JLabel employerEmailError;
    private JLabel result;

    public ChangeEmployerData(Employer employer, EmployerService employerService) {
        setTitle("Работодатель: " + employer.getName());

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        setVisible(true);
        setLocationRelativeTo(null);
        setSize(new Dimension(600, 400));

        JLabel label = new JLabel("Название:");
        label.setFont(new Font("Arial", Font.PLAIN, 14));
        label.setBounds(10, 11, 89, 14);
        contentPanel.add(label);

        JLabel label_1 = new JLabel("Телефон:");
        label_1.setFont(new Font("Arial", Font.PLAIN, 14));
        label_1.setBounds(10, 36, 89, 14);
        contentPanel.add(label_1);

        JLabel label_2 = new JLabel("Адрес:");
        label_2.setFont(new Font("Arial", Font.PLAIN, 14));
        label_2.setBounds(10, 61, 89, 14);
        contentPanel.add(label_2);

        JLabel label_3 = new JLabel("Почта:");
        label_3.setFont(new Font("Arial", Font.PLAIN, 14));
        label_3.setBounds(10, 86, 89, 14);
        contentPanel.add(label_3);

        nameField = new JTextField();
        nameField.setText(employer.getName());
        nameField.setFont(new Font("Arial", Font.PLAIN, 14));
        nameField.setBounds(91, 9, 200, 20);
        contentPanel.add(nameField);
        nameField.setColumns(10);

        phoneField = new JTextField();
        phoneField.setText(employer.getPhone());
        phoneField.setFont(new Font("Arial", Font.PLAIN, 14));
        phoneField.setColumns(10);
        phoneField.setBounds(91, 34, 200, 20);
        contentPanel.add(phoneField);

        addressField = new JTextField();
        addressField.setText(employer.getAddress());
        addressField.setFont(new Font("Arial", Font.PLAIN, 14));
        addressField.setColumns(10);
        addressField.setBounds(91, 59, 200, 20);
        contentPanel.add(addressField);

        emailField = new JTextField();
        emailField.setText(employer.getEmail());
        emailField.setFont(new Font("Arial", Font.PLAIN, 14));
        emailField.setColumns(10);
        emailField.setBounds(91, 86, 200, 20);
        contentPanel.add(emailField);

        JButton button = new JButton("Сохранить");
        button.addActionListener(arg0 -> {
            List<Integer> errors = checkEmployer();

            if (0 == errors.size()) {
                employerService.create(fillEmployer(employer));
                result.setText("Данные успешно сохранены");
            } else {
                for (Integer error : errors) {
                    switch (error) {
                        case 2:
                            employerEmailError.setText("Вы неверно указали электронный адрес");
                            break;
                        case 1:
                            employerPhoneError.setText("Вы неверно указали телефон");
                            break;
                    }
                }
            }
        });
        button.setFont(new Font("Arial", Font.PLAIN, 14));
        button.setBounds(10, 128, 109, 23);
        contentPanel.add(button);

        employerPhoneError = new JLabel("");
        employerPhoneError.setBounds(301, 34, 273, 20);
        contentPanel.add(employerPhoneError);

        employerEmailError = new JLabel("");
        employerEmailError.setBounds(301, 87, 273, 20);
        contentPanel.add(employerEmailError);

        result = new JLabel("");
        result.setBounds(128, 130, 273, 20);
        contentPanel.add(result);

        JButton button_1 = new JButton("Закрыть");
        button_1.addActionListener(arg0 -> setVisible(false));
        button_1.setFont(new Font("Arial", Font.PLAIN, 14));
        button_1.setBounds(10, 331, 89, 23);
        contentPanel.add(button_1);
    }

    private Employer fillEmployer(Employer employer) {
        employer.setName(nameField.getText());
        employer.setPhone(phoneField.getText());
        employer.setAddress(addressField.getText());
        employer.setEmail(emailField.getText());

        return employer;
    }

    private java.util.List<Integer> checkEmployer() {

        List<Integer> errors = new ArrayList<>();

        if (!Errors.checkPhoneNumber(phoneField.getText())) {
            errors.add(Errors.EMPLOYER_PHONE_ERROR);
        }

        if (!Errors.checkEmail(emailField.getText())) {
            errors.add(EMPLOYER_EMAIL_ERROR);
        }

        return errors;
    }
}
