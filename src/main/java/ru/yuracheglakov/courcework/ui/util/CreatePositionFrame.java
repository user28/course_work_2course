package ru.yuracheglakov.courcework.ui.util;

import ru.yuracheglakov.courcework.domain.Position;
import ru.yuracheglakov.courcework.services.PositionService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreatePositionFrame extends JFrame {
    private JTextField textField;
    private JTextField textField_1;

    public CreatePositionFrame(PositionService positionService, final CreateVacancyFrame parent) {
        setTitle("Создание должности");
        setLocationRelativeTo(null);
        setVisible(true);
        setSize(new Dimension(400, 300));

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JLabel lblNewLabel = new JLabel("Название:");
        lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        lblNewLabel.setBounds(10, 11, 86, 14);
        contentPanel.add(lblNewLabel);

        textField = new JTextField();
        textField.setBounds(91, 9, 359, 20);
        contentPanel.add(textField);
        textField.setColumns(10);

        JLabel label = new JLabel("Описание:");
        label.setFont(new Font("Arial", Font.PLAIN, 14));
        label.setBounds(10, 47, 86, 14);
        contentPanel.add(label);

        textField_1 = new JTextField();
        textField_1.setBounds(91, 45, 359, 125);
        contentPanel.add(textField_1);
        textField_1.setColumns(10);

        JButton button = new JButton("Создать");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                positionService.save(fillPosition(positionService));

                parent.fillJListPositions(positionService);

                setVisible(false);
            }
        });
        button.setFont(new Font("Arial", Font.PLAIN, 14));
        button.setBounds(10, 186, 89, 23);
        contentPanel.add(button);
    }

    private Position fillPosition(PositionService positionService) {
        Position position = new Position();

        position.setName(textField.getText());
        position.setComment(textField_1.getText());

        return position;
    }
}
