package ru.yuracheglakov.courcework.ui.util;

import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Response;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.enums.ResponseStatus;
import ru.yuracheglakov.courcework.exceptions.ServiceException;
import ru.yuracheglakov.courcework.services.ResponseService;
import ru.yuracheglakov.courcework.services.VacancyService;

import javax.swing.*;
import java.awt.*;

public class CreateResponseByEmployer extends JFrame {

    private JTextArea comment;

    public CreateResponseByEmployer(Applicant applicant, Employer employer, ResponseService responseService, VacancyService vacancyService) {
        setTitle("Предложить вакансию");
        setVisible(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        setSize(new Dimension(800, 600));

        JLabel result = new JLabel("");
        result.setBounds(10, 190, 784, 30);
        contentPanel.add(result);
        result.setFont(new Font("Arial", Font.PLAIN, 14));

        JLabel label = new JLabel("Вакансия:");
        label.setFont(new Font("Arial", Font.PLAIN, 14));
        label.setBounds(10, 11, 121, 25);
        contentPanel.add(label);

        JLabel label_1 = new JLabel("Комментарий:");
        label_1.setFont(new Font("Arial", Font.PLAIN, 14));
        label_1.setBounds(10, 59, 121, 25);
        contentPanel.add(label_1);


        comment = new JTextArea();
        comment.setBounds(129, 47, 645, 100);
        contentPanel.add(comment);

        JComboBox vacancies = new JComboBox();
        vacancyService.findAll(employer).stream().map(vacancy -> vacancy.getPosition().getName()).forEach(vacancies::addItem);
        vacancies.setBounds(129, 14, 209, 25);
        contentPanel.add(vacancies);


        JButton cancel = new JButton("Отмена");
        cancel.addActionListener(e -> setVisible(false));
        cancel.setFont(new Font("Arial", Font.PLAIN, 14));
        cancel.setBounds(170, 181, 150, 25);
        contentPanel.add(cancel);

        JButton button = new JButton("Предложить");
        button.addActionListener(e -> {
            Vacancy vacancy = null;
            try {
                vacancy = vacancyService.find((String) vacancies.getSelectedItem(), employer);
            } catch (ServiceException e1) {
                e1.printStackTrace();
            }
            if (check(applicant, responseService, vacancy)) {
                responseService.save(fillResponse(applicant, vacancy));
                result.setText("Предложение отправлено.");
                button.setText("Закрыть");
                cancel.setEnabled(false);
                button.addActionListener(e1 -> setVisible(false));
            } else {
                result.setText("Вы уже предлагали такую вакансию.");
            }
        });
        button.setFont(new Font("Arial", Font.PLAIN, 14));
        button.setBounds(10, 181, 150, 25);
        contentPanel.add(button);

    }

    private Response fillResponse(Applicant applicant, Vacancy vacancy) {
        Response response = new Response();

        response.setEmployerStatus(ResponseStatus.YES);
        response.setAppStatus(ResponseStatus.NULL);
        response.setVacancy(vacancy);
        response.setComment(comment.getText());
        response.setApplicant(applicant);

        return response;
    }

    private boolean check(Applicant applicant, ResponseService responseService, Vacancy vacancy) {
        if (null == vacancy)
            return false;
        return !responseService.isExist(vacancy, applicant);
    }


}
