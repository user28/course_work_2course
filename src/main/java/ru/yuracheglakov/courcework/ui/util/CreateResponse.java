package ru.yuracheglakov.courcework.ui.util;

import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Response;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.enums.ResponseStatus;
import ru.yuracheglakov.courcework.services.ResponseService;

import javax.swing.*;
import java.awt.*;

public class CreateResponse extends JFrame {
    public CreateResponse(Vacancy vacancy, Applicant applicant, ResponseService responseService) {

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        setVisible(true);
        setSize(new Dimension(600, 430));
        setBounds(0, 0, 600, 430);

        JLabel label = new JLabel("Комментарий (необязатально):");
        label.setFont(new Font("Arial", Font.PLAIN, 14));
        label.setBounds(10, 11, 287, 30);
        contentPanel.add(label);


        JButton button_1 = new JButton("Отмена");
        button_1.setFont(new Font("Arial", Font.PLAIN, 14));
        button_1.setBounds(460, 320, 133, 23);
        button_1.addActionListener(e -> setVisible(false));
        contentPanel.add(button_1);

        JTextArea textArea = new JTextArea();
        textArea.setBounds(10, 36, 594, 70);
        contentPanel.add(textArea);

        JLabel lblNewLabel = new JLabel("Данные о вакансии:");
        lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        lblNewLabel.setBounds(10, 117, 257, 14);
        contentPanel.add(lblNewLabel);

        JLabel vacancyPosition = new JLabel("");
        vacancyPosition.setText("Должность: " + vacancy.getPosition().getName());
        vacancyPosition.setFont(new Font("Arial", Font.PLAIN, 14));
        vacancyPosition.setBounds(41, 142, 563, 20);
        contentPanel.add(vacancyPosition);

        JLabel vacancySalary = new JLabel("");
        vacancySalary.setText("Зарплата: " + vacancy.getSalary() + "р/мес");
        vacancySalary.setFont(new Font("Arial", Font.PLAIN, 14));
        vacancySalary.setBounds(41, 173, 563, 20);
        contentPanel.add(vacancySalary);

        JLabel vacancyEmployer = new JLabel("");
        vacancyEmployer.setText("Работодатель: " + vacancy.getEmployer().getName());
        vacancyEmployer.setFont(new Font("Arial", Font.PLAIN, 14));
        vacancyEmployer.setBounds(41, 204, 563, 20);
        contentPanel.add(vacancyEmployer);

        JLabel vacancySchedule = new JLabel("");
        vacancySchedule.setText("Рабочий график: " + vacancy.getWorkShedule().getSchedule());
        vacancySchedule.setFont(new Font("Arial", Font.PLAIN, 14));
        vacancySchedule.setBounds(41, 235, 563, 20);
        contentPanel.add(vacancySchedule);

        JLabel result = new JLabel("");
        result.setFont(new Font("Arial", Font.PLAIN, 14));
        result.setBounds(10, 350, 594, 14);
        contentPanel.add(result);

        JButton button = new JButton("Откликнуться");
        button.setFont(new Font("Arial", Font.PLAIN, 14));
        button.addActionListener(e -> {
            Response response = responseService.save(fillResponse(textArea, vacancy, applicant));

            if (null != response) {
                result.setText("Данные успешно сохранены. Ожидайте ответа.");
                button_1.setEnabled(false);
                button.setText("Закрыть");
                button.addActionListener(e1 -> setVisible(false));
            } else {
                result.setText("Произошла ошибка. Обратитесь к разработчику.");
            }
        });
        button.setBounds(310, 320, 133, 23);
        contentPanel.add(button);

        setTitle("Создание отклика на вакансию: " + vacancy.getPosition().getName());
    }

    private Response fillResponse(JTextArea comment, Vacancy vacancy, Applicant applicant) {
        Response response = new Response();

        response.setApplicant(applicant);
        response.setAppStatus(ResponseStatus.YES);
        response.setComment(comment.getText());
        response.setVacancy(vacancy);
        response.setEmployerStatus(ResponseStatus.NULL);

        return response;
    }
}
