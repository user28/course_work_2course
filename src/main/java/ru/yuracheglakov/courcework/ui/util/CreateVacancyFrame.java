package ru.yuracheglakov.courcework.ui.util;

import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Position;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.domain.WorkShedule;
import ru.yuracheglakov.courcework.enums.VacancyStatus;
import ru.yuracheglakov.courcework.exceptions.ServiceException;
import ru.yuracheglakov.courcework.services.PositionService;
import ru.yuracheglakov.courcework.services.VacancyService;
import ru.yuracheglakov.courcework.services.WorkSheduleService;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.*;
import java.time.LocalDateTime;
import java.util.List;

public class CreateVacancyFrame extends JFrame {
    private JTextField textField;
    private JList workShedule;
    private JList positions;
    private Employer employer;

    public CreateVacancyFrame(VacancyService vacancyService, PositionService positionService, WorkSheduleService workSheduleService, Employer employer) {
        this.employer = employer;

        setResizable(true);
        setTitle("Создание вакансии");
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setVisible(true);
        setLocationRelativeTo(null);

        JPanel contentPanel = new JPanel();
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JLabel lblNewLabel = new JLabel("Выберите должность или создайте новую:");
        lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        lblNewLabel.setBounds(10, 11, 301, 14);
        contentPanel.add(lblNewLabel);

        positions = fillJListPositions(positionService);
        positions.setLayoutOrientation(JList.VERTICAL);
        positions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        positions.setBounds(10, 36, 233, 30);

        JScrollPane scrollPane = new JScrollPane(positions);
        scrollPane.setLocation(10, 42);
        scrollPane.setSize(233, 30);
        scrollPane.setPreferredSize(new Dimension(233, 30));
        contentPanel.add(scrollPane);

        JButton createPositionButton = new JButton("Создать");
        createPositionButton.setBounds(253, 36, 89, 30);
        createPositionButton.addActionListener(e -> new CreatePositionFrame(positionService, this));
        contentPanel.add(createPositionButton);

        textField = new JTextField();
        textField.setBounds(10, 102, 233, 30);
        ((AbstractDocument) textField.getDocument()).setDocumentFilter(new DigitFilterDocument());
        contentPanel.add(textField);
        textField.setColumns(10);

        JLabel label = new JLabel("Укажите зарплату:");
        label.setFont(new Font("Arial", Font.PLAIN, 14));
        label.setBounds(10, 77, 142, 14);
        contentPanel.add(label);

        workShedule = fillJListWorkShedules(workSheduleService);
        workShedule.setLayoutOrientation(JList.VERTICAL_WRAP);
        workShedule.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        workShedule.setBounds(10, 168, 233, 30);

        JScrollPane workScheduleScroll = new JScrollPane(workShedule);
        workScheduleScroll.setLocation(10, 168);
        workScheduleScroll.setSize(233, 30);
        workScheduleScroll.setPreferredSize(new Dimension(233, 30));
        contentPanel.add(workScheduleScroll);

        JLabel label_1 = new JLabel("Выберите рабочий график:");
        label_1.setFont(new Font("Arial", Font.PLAIN, 14));
        label_1.setBounds(10, 143, 301, 14);
        contentPanel.add(label_1);

        JButton saveVacancy = new JButton("Сохранить вакансию");
        saveVacancy.addActionListener(arg0 -> {
            try {
                vacancyService.save(fillVacancy(positionService, workSheduleService));
                setVisible(false);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        });
        saveVacancy.setBounds(10, 209, 160, 30);
        contentPanel.add(saveVacancy);

        this.setSize(new Dimension(485, 351));
    }

    private Vacancy fillVacancy(PositionService positionService, WorkSheduleService workSheduleService) throws ServiceException {
        Vacancy vacancy = new Vacancy();

        vacancy.setDate(LocalDateTime.now());
        vacancy.setEmployer(employer);
        vacancy.setPosition(positionService.find(positions.getSelectedValue().toString()));
        vacancy.setSalary(Integer.valueOf(textField.getText()));
        vacancy.setStatus(VacancyStatus.ACTIVE);
        vacancy.setWorkShedule(workSheduleService.find((long) workShedule.getSelectedIndex() + 1));

        return vacancy;
    }

    private List<Position> getPositions(PositionService positionService) {
        return positionService.findAll();
    }

    private List<WorkShedule> getWorkShedules(WorkSheduleService workSheduleService) {
        return workSheduleService.findAll();
    }

    public JList fillJListPositions(PositionService positionService) {

        List<Position> positions = getPositions(positionService);

        DefaultListModel<String> defaultListModel = new DefaultListModel<>();

        int index = 0;
        for (Position position : positions) {
            defaultListModel.add(index, position.getName());
            index++;
        }

        return new JList<>(defaultListModel);

    }

    private JList fillJListWorkShedules(WorkSheduleService workSheduleService) {

        List<WorkShedule> workShedules = getWorkShedules(workSheduleService);

        DefaultListModel<String> defaultListModel = new DefaultListModel<>();

        int index = 0;
        for (WorkShedule workShedule : workShedules) {
            defaultListModel.add(index, workShedule.getSchedule());
            index++;
        }

        return new JList<>(defaultListModel);

    }

    class DigitFilterDocument extends DocumentFilter {
        @Override
        public void insertString(DocumentFilter.FilterBypass fp
                , int offset, String string, AttributeSet aset)
                throws BadLocationException {
            int len = string.length();
            boolean isValidInteger = true;

            for (int i = 0; i < len; i++) {
                if (!Character.isDigit(string.charAt(i))) {
                    isValidInteger = false;
                    break;
                }
            }
            if (isValidInteger)
                super.insertString(fp, offset, string, aset);
            else
                Toolkit.getDefaultToolkit().beep();
        }

        @Override
        public void replace(DocumentFilter.FilterBypass fp, int offset
                , int length, String string, AttributeSet aset)
                throws BadLocationException {
            int len = string.length();
            boolean isValidInteger = true;

            for (int i = 0; i < len; i++) {
                if (!Character.isDigit(string.charAt(i))) {
                    isValidInteger = false;
                    break;
                }
            }
            if (isValidInteger)
                super.replace(fp, offset, length, string, aset);
            else
                Toolkit.getDefaultToolkit().beep();
        }
    }
}
