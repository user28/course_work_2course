package ru.yuracheglakov.courcework.ui.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.yuracheglakov.courcework.exceptions.ServiceException;
import ru.yuracheglakov.courcework.services.*;
import ru.yuracheglakov.courcework.ui.MainFrame;
import ru.yuracheglakov.courcework.ui.registration.RegistrationFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

@Component
public class AuthFrame extends JFrame {

    private final AuthService authService;

    private final ProcedureInvoker procedureInvoker;

    private final PersonService personService;

    private final EmployerService employerService;

    private final WorkSheduleService workSheduleService;

    private final PositionService positionService;

    private final VacancyService vacancyService;

    private final ResponseService responseService;

    private final ApplicantService applicantService;

    private final EducationService educationService;
    private JLabel errorLabel;

    @Autowired
    public AuthFrame(AuthService authService, ProcedureInvoker procedureInvoker, PersonService personService, EmployerService employerService, WorkSheduleService workSheduleService, PositionService positionService, VacancyService vacancyService, ResponseService responseService, ApplicantService applicantService, EducationService educationService) {

        this.authService = authService;
        this.procedureInvoker = procedureInvoker;
        this.personService = personService;
        this.employerService = employerService;
        this.workSheduleService = workSheduleService;
        this.positionService = positionService;
        this.vacancyService = vacancyService;
        this.responseService = responseService;
        this.applicantService = applicantService;
        this.educationService = educationService;

        setTitle("Бюро трудоустройств");
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);

        JPanel contentPane = new JPanel();
        getContentPane().add(contentPane, BorderLayout.CENTER);
        contentPane.setLayout(null);

        JPasswordField passwordField = new JPasswordField();
        passwordField.setBounds(367, 261, 150, 25);
        contentPane.add(passwordField);

        JLabel password_label = new JLabel("Пароль:");
        password_label.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        password_label.setBounds(307, 259, 50, 25);
        contentPane.add(password_label);

        JLabel login_label = new JLabel("Электронная почта:");
        login_label.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        login_label.setBounds(231, 224, 131, 25);
        contentPane.add(login_label);

        JTextField loginField = new JTextField();
        loginField.setBounds(367, 225, 150, 25);
        contentPane.add(loginField);

        JLabel description_label = new JLabel("Добро пожаловать в бюротрудоустройств!\r\n Войдите или зарегистрируйтесь.");
        description_label.setHorizontalAlignment(SwingConstants.CENTER);
        description_label.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        description_label.setBounds(119, 152, 580, 40);
        contentPane.add(description_label);

        JLabel label = new JLabel("Регистрация");
        label.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        label.setBounds(699, 540, 85, 14);
        label.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setVisible(false);
                new RegistrationFrame(procedureInvoker, personService, employerService, vacancyService, workSheduleService, positionService, responseService, applicantService, educationService);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        contentPane.add(label);

        JButton button = new JButton("Войти");
        button.addActionListener(e -> {
            Long personId = AuthService.isCorrect(loginField.getText(), passwordField.getPassword());
            if (0 != personId) {
                setVisible(false);
                errorLabel.setText("");
                try {
                    new MainFrame(personService, personId, vacancyService, workSheduleService, positionService, employerService, responseService, applicantService, educationService);
                } catch (ServiceException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            } else {
                errorLabel.setText("Ошибка входа. Повторите попытку");
            }
        });
        button.setBounds(398, 297, 89, 23);
        contentPane.add(button);

        errorLabel = new JLabel("");
        errorLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        errorLabel.setBounds(231, 373, 428, 25);
        errorLabel.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(errorLabel);
        setSize(new Dimension(800, 600));
    }
}
