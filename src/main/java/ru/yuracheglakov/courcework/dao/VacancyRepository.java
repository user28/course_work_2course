package ru.yuracheglakov.courcework.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Vacancy;
import ru.yuracheglakov.courcework.enums.VacancyStatus;

import java.util.List;
import java.util.Optional;

public interface VacancyRepository extends JpaRepository<Vacancy, Long> {
    List<Vacancy> findAllByStatusIs(VacancyStatus vacancyStatus);

    List<Vacancy> findAllByEmployer(Employer employer);

    Optional<Vacancy> findByPosition_NameAndEmployer(String name, Employer employer);
}
