package ru.yuracheglakov.courcework.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yuracheglakov.courcework.domain.Employer;

public interface EmployerRepository extends JpaRepository<Employer, Long> {
    Employer findByContactPerson_Id(Long id);
}
