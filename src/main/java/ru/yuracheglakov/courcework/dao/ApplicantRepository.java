package ru.yuracheglakov.courcework.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Person;

import java.util.Optional;

public interface ApplicantRepository extends JpaRepository<Applicant, Long> {

    Optional<Applicant> findByPerson(Person person);

}
