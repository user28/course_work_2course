package ru.yuracheglakov.courcework.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yuracheglakov.courcework.domain.Applicant;
import ru.yuracheglakov.courcework.domain.Employer;
import ru.yuracheglakov.courcework.domain.Response;
import ru.yuracheglakov.courcework.domain.Vacancy;

import java.util.List;
import java.util.Optional;

public interface ReponseRepository extends JpaRepository<Response, Long> {

    Integer countAllByVacancy(Vacancy vacancy);

    Optional<Response> findByVacancyAndApplicant(Vacancy vacancy, Applicant applicant);

    List<Response> findAllByVacancy_Employer(Employer employer);

    List<Response> findAllByVacancyAndVacancy_Employer(Vacancy vacancy, Employer employer);

    Boolean existsByApplicantAndVacancy(Applicant applicant, Vacancy vacancy);
}
