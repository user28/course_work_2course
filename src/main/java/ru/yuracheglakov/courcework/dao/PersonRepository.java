package ru.yuracheglakov.courcework.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yuracheglakov.courcework.domain.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
