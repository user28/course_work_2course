package ru.yuracheglakov.courcework.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yuracheglakov.courcework.domain.WorkShedule;

public interface WorkSheduleRepository extends JpaRepository<WorkShedule, Long> {
}
